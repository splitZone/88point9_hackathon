import Vue from 'vue/dist/vue.js';
import Router from 'vue-router';
import App from '@/App';
import Discover from '@/Discover';
import Home from '@/Home';
import Settings from '@/Settings';

Vue.use(Router);
/* eslint-disable no-new */
export default new Router({
  routes: [
    {
      path: '/',
      name: 'App',
      component: App
    },
    {
      path: '/discover',
      name: 'discover',
      component: Discover
    },
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    { 
      path: '/settings',
      name: 'settings',
      component: Settings
    }
  ]
});

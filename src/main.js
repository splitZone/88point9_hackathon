import Vue from 'vue/dist/vue.js';
import App from './App.vue';
import Discover from './Discover.vue';
import BootstrapVue from 'bootstrap-vue';
import router from './router';
import Home from './Home.vue';
import Settings from './Settings.vue';

Vue.use(BootstrapVue);
Vue.config.productionTip = true;

new Vue({
  el: '#app',
  router,
  components: { App, Home, Discover }
});

# Rifft

Music collaboration platform for MKE artists.

## Public URL

### https://sharp-shannon-31f19f.netlify.com/

## Inspiration
88.9FM Hackathon

## Getting Started

To get Rifft running on your local machine run the following in terminal:

1. ```git clone https://begooddogood@bitbucket.org/splitZone/88point9_hackathon.git```
2. ```cd 88point9_hackathon```
3. ```git checkout -b [your new dev branch name] ```
4. ```npm install```
5. ```npm run serve```

When you are ready to merge your changes run the following in terminal:

1. ```git add . ```
2. ```git commit -m "[your commit message]"```
2. ```git push origin [your dev branch name]```

then [Create Pull Request](https://bitbucket.org/splitZone/88point9_hackathon/pull-requests/) and do the following steps:

1. Select [your dev branch name] from left dropdown
2. Select master from right dropdown
3. Write a short description
4. check 'close branch' checkbox
5. click 'Create Pull Request' button

When the merge has been approved you can switch back to the master branch on your local machine and do run the following in terminal:

1. ```git checkout master```
2. ```git pull origin master```

To remove all local branches except 'master' run the following in terminal:

```git branch | grep -v 'master' | xargs git branch -d```

## Built With
* [vuejs](https://vuejs.org/v2/guide/) - frontend JS framework
* [figma](https://www.figma.com/) - collaborative design tool
* [notion](https://www.notion.so/) - collaborative project management tool
* [Airtable](https://airtable.com/product) - spreadsheet meets database
* [netlify](https://www.netlify.com/features/) - build, deply, and manage modern web projects

## Contributors
+ **Steven Price** - [stevenjacobprice.com](https://www.stevenjacobprice.com/)
+ **Zachary Blasczyk**
+ **Michelle Chan**
+ **Jess Izumi**
+ **Chandler Luhowskyj**
+ **Vanessa Rodriguez**
+ **Chris Carpenter**

## License

This project is licensed under the MIT License - see the [LICENSE.md](https://raw.githubusercontent.com/BTBTravis/mke-bus-graphql/master/LICENSE.md) file for details